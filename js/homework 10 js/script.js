'use strict'
const typePassword = document.getElementsByClassName('type-pass')[0];
const submitPass = document.getElementsByClassName('submit-pass')[0];
const showIcon = document.getElementsByClassName('fas fa-eye icon-password')[0];
const hideIcon = document.getElementsByClassName('fas fa-eye-slash icon-password')[0];

function togglePassword(){
    showIcon.addEventListener('click',() =>{
        if(typePassword.getAttribute('type')==='password'){
            typePassword.setAttribute('type','text')

        }else {
            typePassword.setAttribute('type','password')

        }
    })
    hideIcon.addEventListener('click',() =>{
        if(submitPass.getAttribute('type')==='password'){
            submitPass.setAttribute('type','text')
        }else {
            submitPass.setAttribute('type','password')
        }
    })
}
togglePassword();
function greeting() {
    const btn = document.getElementsByClassName('btn')[0];
    btn.addEventListener('click', () =>{
        if (typePassword.value === submitPass.value){
            alert('You are welcome!')
        } else {
            invalidData();
        }
    })
}
greeting()

function invalidData() {
    const submitPassWrap = document.getElementsByClassName('input-wrapper')[1]
    const invalidData = document.createElement('span')
    invalidData.innerText = 'Нужно ввести одинаковые значения'
    invalidData.classList.add('invalid-data')
    submitPassWrap.after(invalidData)
}

