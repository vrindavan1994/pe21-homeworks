"use strict";
let userNum = +prompt("What's your Fibonacci's num?");
function fibonacci(n) {
    if (n === 0) {
        return 0;
    }else if (n < 0){
        return fibonacci(n + 2) - fibonacci(n + 1);
    } else if (n !== 1) {
        return fibonacci(n - 1) + fibonacci(n - 2);
    } else {
        return 1;
    }
}
console.log(fibonacci(userNum));