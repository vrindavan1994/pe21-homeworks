$(".btn-toggle").click(function () {
    $(".container-top-rated").slideToggle("slow", function () {
        if ($(this).is(':hidden')) {
            $('.btn-toggle').html('Show Top Rated');
        } else {
            $('.btn-toggle').html('Hide Top Rated');
        }
    });
});