const allImages = ['img-1.jpg','img-2.jpg','img-3.JPG','img-4.png'];
const img = document.querySelector('.image-to-show');
let i = 1;
const controlBtn = document.getElementById('slide-control');
let playing = true;
const slideInterval = setInterval(()=>{iteration()},10000)

function slides(){
    controlBtn.addEventListener('click', () =>{
    if (playing){
        controlBtn.innerText = 'Play';
        playing = false;
        clearInterval(slideInterval)
    } else {
        controlBtn.innerHTML = 'Pause';
        playing = true;
        setInterval('iteration()',10000);
    }
})
}
function iteration() {
    img.src = 'img/' + allImages[i];
    i++;

    if (i === allImages.length){
        i = 0;
    }
}
window.onload = slides;
