"use strict";
function count(num1 = +prompt("Enter num1"), num2 = +prompt("Enter num2"), operator = prompt("choose an operator: - + / *")){
    switch (operator) {
        case "*":
            return num1 * num2;
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "/":
            return num1 / num2;
    }
}

console.log(count());

//функция нужна чтобы выполнять кусочек кода много раз не повторяя все написание кода снова и снова
//аргументы нужны для того, что бы функция понимала с какой входящей информацией она будет работать 