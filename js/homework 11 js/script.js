'use strict';
const btns = document.querySelectorAll('.btn');
document.addEventListener('keydown', highlightBtn)
function highlightBtn(e) {
    btns.forEach(el => {
        if (e.key.toLowerCase() === el.textContent.toLowerCase()) {
            el.classList.toggle('blue-btn');
        } else {
            el.classList.remove('blue-btn');
        }
    });
}
