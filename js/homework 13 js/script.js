const btn = document.querySelector('.change-color'),
      bgChange = document.querySelector('.perspective');
let color;
if (localStorage.getItem('background') !== null) {
    color = localStorage.background;
    bgChange.style.background = color;
}
btn.addEventListener('click', () =>{
        if (color ==='black'){
            color = 'white';
            bgChange.style.background = color;
            localStorage.setItem('background','white');
        } else {
            color = 'black';
            bgChange.style.background = color;
            localStorage.setItem('background','black');
        }

});


