function createNewUser(firstName = prompt("What's your name?"), lastName = prompt("What's your last name?")) {
    return {
        firstName,
        lastName,
        getLogin: function () {
            return  firstName[0].toLowerCase() + lastName.toLowerCase()
        }
    };
}

const newUser = createNewUser();
console.log(newUser.getLogin());