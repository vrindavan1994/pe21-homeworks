$(function () {
    let filter = $('[data-filter]');
    filter.on('click', function () {
        let title = $(this).data('filter')
        if (title === 'all'){
            $('[data-tab]').removeClass('hide')
            const btnLoadMore = $('<button>+ Load More</button>')
            $('.container-amazing-work').append($(btnLoadMore))
            $(btnLoadMore).addClass('btn-load-more')
            $('.btn-none').hide();
            $(btnLoadMore).on('click', function () {
                $('.btn-none').show();
                $(btnLoadMore).hide()
            })
        }else {
            $('[data-tab]').each(function () {

                let activeContent = $(this).data('tab')

                if (activeContent !== title){
                    $(this).addClass('hide')
                } else {
                    $(this).removeClass('hide')
                }
            })
        }

    });
});



