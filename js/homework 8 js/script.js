'use strict'
const greenInput = document.getElementsByClassName('price-field')[0];
const priceSpan = document.getElementsByClassName('values')[0];
const invalidData = document.getElementsByClassName('invalid-text')[0]

greenInput.addEventListener('focus', function () {
    this.classList.add('green')
})
greenInput.addEventListener('blur', event => {
    if ((parseInt(greenInput.value)) <= 0 || (greenInput.value) === '' || (greenInput.value.match(/[^0-9]|^0/))) {
        greenInput.classList.remove('green-bg-input')
        greenInput.classList.add('invalid-data')
        invalidData.innerText = 'Please enter correct data'
    } else {
        invalidData.innerText = ''
        priceSpan.innerText = `Current Price: ${event.currentTarget.value}`
        greenInput.classList.remove('invalid-data')
        greenInput.classList.add('green-bg-input')
    }
    let butt = document.createElement('input')
    butt.setAttribute('type', 'reset')
    priceSpan.append(butt)
    butt.addEventListener('click', () => {
        butt.classList.add('hide')
        priceSpan.classList.add('hide')
        greenInput.value = 0
    })

});